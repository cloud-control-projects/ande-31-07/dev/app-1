import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { Breadcrumb } from "antd";
import { OwnerTableWithFilter } from "./OwnerTableWithFilter";
import { OwnerTableWithFilterEditor } from "./OwnerTableWithFilterEditor";
import { BreadcrumbContext } from "../../../core/screen/BreadcrumbContext";
import { usePageTitle } from "../../../core/screen/usePageTitle";
import { useIntl } from "react-intl";

export function OwnerTableWithFilterScreenLayout() {
  const intl = useIntl();
  usePageTitle(intl.formatMessage({ id: "screen.OwnerTableWithFilter" }));

  const { recordId } = useParams();
  const [breadcrumbItems, setBreadcrumbItems] = useState<string[]>([]);

  return (
    <>
      {recordId && (
        <Breadcrumb className="crud-screen-breadcrumb">
          {breadcrumbItems.map((item, index) => (
            <Breadcrumb.Item key={`breadcrumb${index}`}>{item}</Breadcrumb.Item>
          ))}
        </Breadcrumb>
      )}

      <BreadcrumbContext.Provider value={setBreadcrumbItems}>
        <div style={{ display: recordId ? "none" : "block" }}>
          <OwnerTableWithFilter />
        </div>
        {recordId && (
          <OwnerTableWithFilterEditor
            refetchQueries={["Get_Owner_List_With_Filter"]}
          />
        )}
      </BreadcrumbContext.Provider>
    </>
  );
}
