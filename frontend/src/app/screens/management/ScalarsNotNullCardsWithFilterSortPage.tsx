import { useMemo, ReactNode, useState, useCallback, useEffect } from "react";
import { useQuery, useMutation } from "@apollo/client";
import { ApolloError } from "@apollo/client/errors";
import { ResultOf, VariablesOf } from "@graphql-typed-document-node/core";
import {
  Button,
  Modal,
  message,
  Row,
  Col,
  Form,
  Input,
  InputNumber,
  Card,
  Empty,
  Space,
  Spin,
  Badge,
  Pagination,
  Select
} from "antd";
import { useForm } from "antd/lib/form/Form";
import { serializeVariables } from "../../../core/transform/model/serializeVariables";
import { DatePicker, TimePicker } from "@amplicode/react";
import {
  DeleteOutlined,
  LoadingOutlined,
  EditOutlined,
  PlusOutlined,
  CloseCircleOutlined,
  UpOutlined,
  DownOutlined,
  ArrowUpOutlined,
  ArrowDownOutlined
} from "@ant-design/icons";
import { useNavigate, useSearchParams } from "react-router-dom";
import { FormattedMessage, useIntl } from "react-intl";
import { gql } from "@amplicode/gql";
import { ValueWithLabel } from "../../../core/crud/ValueWithLabel";
import { useDeleteItem } from "../../../core/crud/useDeleteItem";
import { GraphQLError } from "graphql/error/GraphQLError";
import { FetchResult } from "@apollo/client/link/core";
import { RequestFailedError } from "../../../core/crud/RequestFailedError";
import { deserialize } from "../../../core/transform/model/deserialize";
import { getNotNullScalarsTestEntityDisplayName } from "../../../core/display-name/getNotNullScalarsTestEntityDisplayName";
import { useBreadcrumbItem } from "../../../core/screen/useBreadcrumbItem";
import { NamePath } from "antd/lib/form/interface";
import {
  SortDirection,
  NotNullScalarsTestEntityOrderByProperty
} from "../../../gql/graphql";
import { DefaultOptionType } from "antd/lib/select";

const REFETCH_QUERIES = [
  "Get_NN_Scalars_Test_Entity_List_With_Filter_Page_Sort"
];

const NOT_NULL_SCALARS_TEST_ENTITY_LIST_WITH_FILTER_SORT_PAGE = gql(`
  query Get_NN_Scalars_Test_Entity_List_With_Filter_Page_Sort($filter: NotNullScalarsTestEntityFilterInput, $page: OffsetPageInput, $sort: [NotNullScalarsTestEntityOrderByInput]) {
  notNullScalarsTestEntityList(filter: $filter, page: $page, sort: $sort) {
    content {
      id
      bigDecimalNotNull
      bigIntNotNull
      dateTestNotNull
      localDateNotNull
      localDateTimeNotNull
      localTimeNotNull
      offsetDateTimeNotNull
      offsetTimeNotNull
      stringNotNull
      urlNotNull
    }
    totalElements
  }
}
`);

const DELETE_NOT_NULL_SCALARS_TEST_ENTITY = gql(`
  mutation Delete_NN_Scalars($id: ID!) {
    deleteNotNullScalarsTestEntity(id: $id)
  }
`);

const DEFAULT_PAGE_SIZE = 10;

export function ScalarsNotNullCardsWithFilterSortPage() {
  const intl = useIntl();
  useBreadcrumbItem(
    intl.formatMessage({ id: "screen.ScalarsNotNullCardsWithFilterSortPage" })
  );

  const [searchParams, setSearchParams] = useSearchParams();
  // Selection state is initialized to URL search params
  const [selectionState, setSelectionState] = useState<QueryVariablesType>(
    searchParamsToState(searchParams)
  );

  const [initialFilterValues] = useState<QueryVariablesType>(
    extractFilterParams(selectionState)
  );

  useEffect(() => {
    // Whenever selection state is changed, update URL search params accordingly
    setSearchParams(stateToSearchParams(selectionState));
  }, [selectionState, setSearchParams]);

  // Load the items from server. Will be reloaded reactively if one of variable changes
  const {
    loading,
    error,
    data
  } = useQuery(NOT_NULL_SCALARS_TEST_ENTITY_LIST_WITH_FILTER_SORT_PAGE, {
    variables: selectionState
  });

  const items = useMemo(
    () =>
      deserialize(
        data?.notNullScalarsTestEntityList?.content
      ),
    [data?.notNullScalarsTestEntityList?.content]
  );

  const applyPagination = useCallback((current: number, pageSize: number) => {
    setSelectionState(prevState => ({
      ...prevState,
      page: {
        number: current - 1,
        size: pageSize
      }
    }));
  }, []);

  const applySort = useCallback(
    (newSortValue: QueryVariablesType["sort"] | undefined) => {
      setSelectionState(prevState => {
        const newState = { ...prevState };
        if (newSortValue != null) {
          newState.sort = newSortValue;
        } else {
          delete newState.sort;
        }
        return newState;
      });
    },
    []
  );

  const applyFilters = useCallback((filters: QueryVariablesType) => {
    setSelectionState(prevState => {
      const newFilters = serializeVariables(
        NOT_NULL_SCALARS_TEST_ENTITY_LIST_WITH_FILTER_SORT_PAGE,
        filters
      );
      return {
        ...prevState,
        page: {
          number: 0,
          size: prevState.page?.size ?? DEFAULT_PAGE_SIZE
        },
        ...newFilters
      };
    });
  }, []);

  return (
    <div className="narrow-layout">
      <Space direction="vertical" className="card-space">
        <Card>
          <Filters
            onApplyFilters={applyFilters}
            initialFilterValues={initialFilterValues}
          />
        </Card>
        <ButtonPanel onApplySort={applySort} sortValue={selectionState.sort} />
        <Cards items={items} loading={loading} error={error} />
        <Pagination
          current={
            selectionState.page?.number != null
              ? selectionState.page?.number + 1
              : undefined
          }
          pageSize={selectionState.page?.size}
          onChange={applyPagination}
          showSizeChanger
          total={
            data?.notNullScalarsTestEntityList?.totalElements
          }
        />
      </Space>
    </div>
  );
}

const sortBySelectorOptions: DefaultOptionType[] = [
  {
    label: (
      <>
        Big Decimal Not Null (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: NotNullScalarsTestEntityOrderByProperty.BigDecimalNotNull
    })
  },
  {
    label: (
      <>
        Big Decimal Not Null (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: NotNullScalarsTestEntityOrderByProperty.BigDecimalNotNull
    })
  },
  {
    label: (
      <>
        Big Int Not Null (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: NotNullScalarsTestEntityOrderByProperty.BigIntNotNull
    })
  },
  {
    label: (
      <>
        Big Int Not Null (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: NotNullScalarsTestEntityOrderByProperty.BigIntNotNull
    })
  },
  {
    label: (
      <>
        Date Test Not Null (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: NotNullScalarsTestEntityOrderByProperty.DateTestNotNull
    })
  },
  {
    label: (
      <>
        Date Test Not Null (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: NotNullScalarsTestEntityOrderByProperty.DateTestNotNull
    })
  },
  {
    label: (
      <>
        Local Date Not Null (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: NotNullScalarsTestEntityOrderByProperty.LocalDateNotNull
    })
  },
  {
    label: (
      <>
        Local Date Not Null (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: NotNullScalarsTestEntityOrderByProperty.LocalDateNotNull
    })
  },
  {
    label: (
      <>
        Local Date Time Not Null (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: NotNullScalarsTestEntityOrderByProperty.LocalDateTimeNotNull
    })
  },
  {
    label: (
      <>
        Local Date Time Not Null (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: NotNullScalarsTestEntityOrderByProperty.LocalDateTimeNotNull
    })
  },
  {
    label: (
      <>
        Local Time Not Null (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: NotNullScalarsTestEntityOrderByProperty.LocalTimeNotNull
    })
  },
  {
    label: (
      <>
        Local Time Not Null (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: NotNullScalarsTestEntityOrderByProperty.LocalTimeNotNull
    })
  },
  {
    label: (
      <>
        Offset Date Time Not Null (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: NotNullScalarsTestEntityOrderByProperty.OffsetDateTimeNotNull
    })
  },
  {
    label: (
      <>
        Offset Date Time Not Null (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: NotNullScalarsTestEntityOrderByProperty.OffsetDateTimeNotNull
    })
  },
  {
    label: (
      <>
        Offset Time Not Null (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: NotNullScalarsTestEntityOrderByProperty.OffsetTimeNotNull
    })
  },
  {
    label: (
      <>
        Offset Time Not Null (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: NotNullScalarsTestEntityOrderByProperty.OffsetTimeNotNull
    })
  },
  {
    label: (
      <>
        String Not Null (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: NotNullScalarsTestEntityOrderByProperty.StringNotNull
    })
  },
  {
    label: (
      <>
        String Not Null (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: NotNullScalarsTestEntityOrderByProperty.StringNotNull
    })
  },
  {
    label: (
      <>
        Url Not Null (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: NotNullScalarsTestEntityOrderByProperty.UrlNotNull
    })
  },
  {
    label: (
      <>
        Url Not Null (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: NotNullScalarsTestEntityOrderByProperty.UrlNotNull
    })
  }
];

interface ButtonPanelProps {
  onApplySort: (sort: QueryVariablesType["sort"]) => void;
  sortValue?: QueryVariablesType["sort"];
}
/**
 * Button panel above the cards
 */
function ButtonPanel({ onApplySort, sortValue }: ButtonPanelProps) {
  const intl = useIntl();
  const navigate = useNavigate();

  return (
    <Row justify="space-between" gutter={[16, 8]}>
      <Col>
        <Space>
          <Button
            htmlType="button"
            key="create"
            title={intl.formatMessage({ id: "common.create" })}
            type="primary"
            icon={<PlusOutlined />}
            onClick={() => navigate("new")}
          >
            <span>
              <FormattedMessage id="common.create" />
            </span>
          </Button>
        </Space>
      </Col>
      <Col>
        <Select
          value={JSON.stringify(sortValue)}
          className="sort-by-select-width"
          allowClear
          placeholder={intl.formatMessage({ id: "sort.sortBy" })}
          onChange={sortBy => onApplySort(sortBy && JSON.parse(sortBy))}
          options={sortBySelectorOptions}
        />
      </Col>
    </Row>
  );
}

const couldBeHiddenFilters: NamePath[] = [
  ["filter", "localDateTimeNotNullMin"],

  ["filter", "localTimeNotNull"],

  ["filter", "offsetDateTimeNotNullMax"],

  ["filter", "offsetDateTimeNotNullMin"],

  ["filter", "offsetTimeNotNullMax"],

  ["filter", "stringNotNull"],

  ["filter", "urlNotNull"]
];

interface FiltersProps {
  onApplyFilters: (queryVariables: QueryVariablesType) => void;
  initialFilterValues: QueryVariablesType;
}

function Filters({ onApplyFilters, initialFilterValues }: FiltersProps) {
  const [form] = useForm();

  const [showAll, setShowAll] = useState(false);

  const [countHiddenTouchedFilters, setCountHiddenTouchedFilters] = useState(0);
  const [countHiddenInvalideFilters, setCountHiddenInvalideFilters] = useState(
    0
  );

  useEffect(() => {
    form.setFieldsValue(initialFilterValues);
  }, [form, initialFilterValues]);

  const onResetFilters = useCallback(async () => {
    await form.resetFields();
    const filters = await form.validateFields();
    onApplyFilters(filters);
  }, [form, onApplyFilters]);

  return (
    <Form form={form} layout="vertical" onFinish={onApplyFilters}>
      <Form.Item shouldUpdate>
        {() => {
          const newCountHiddenTouchedFilters = showAll
            ? 0
            : couldBeHiddenFilters.filter(filterName =>
                form.isFieldTouched(filterName)
              ).length;
          if (newCountHiddenTouchedFilters !== countHiddenTouchedFilters) {
            setCountHiddenTouchedFilters(newCountHiddenTouchedFilters);
          }

          const newCountHiddenInvalideFilters = showAll
            ? 0
            : couldBeHiddenFilters.filter(
                filterName => form.getFieldError(filterName).length > 0
              ).length;
          if (newCountHiddenInvalideFilters !== countHiddenInvalideFilters) {
            setCountHiddenInvalideFilters(newCountHiddenInvalideFilters);
          }

          return (
            <Row gutter={16}>
              <Col span={6}>
                <Form.Item
                  name={["filter", "bigDecimalNotNullMin"]}
                  label="Big Decimal Not Null Min"
                >
                  <InputNumber type="number" stringMode />
                </Form.Item>
              </Col>

              <Col span={6}>
                <Form.Item
                  name={["filter", "bigIntNotNullMin"]}
                  label="Big Int Not Null Min"
                >
                  <InputNumber type="number" precision={0} stringMode />
                </Form.Item>
              </Col>

              <Col span={6}>
                <Form.Item
                  name={["filter", "dateTestNotNullMin"]}
                  label="Date Test Not Null Min"
                >
                  <DatePicker showTime={{ format: "HH:mm:ss" }} />
                </Form.Item>
              </Col>

              <Col span={6}>
                <Form.Item
                  name={["filter", "localDateNotNullMax"]}
                  label="Local Date Not Null Max"
                >
                  <DatePicker />
                </Form.Item>
              </Col>

              <Col span={6} style={{ display: showAll ? "block" : "none" }}>
                <Form.Item
                  name={["filter", "localDateTimeNotNullMin"]}
                  label="Local Date Time Not Null Min"
                >
                  <DatePicker showTime={{ format: "HH:mm:ss" }} />
                </Form.Item>
              </Col>

              <Col span={6} style={{ display: showAll ? "block" : "none" }}>
                <Form.Item
                  name={["filter", "localTimeNotNull"]}
                  label="Local Time Not Null"
                >
                  <TimePicker />
                </Form.Item>
              </Col>

              <Col span={6} style={{ display: showAll ? "block" : "none" }}>
                <Form.Item
                  name={["filter", "offsetDateTimeNotNullMax"]}
                  label="Offset Date Time Not Null Max"
                >
                  <DatePicker showTime={{ format: "HH:mm:ss" }} />
                </Form.Item>
              </Col>

              <Col span={6} style={{ display: showAll ? "block" : "none" }}>
                <Form.Item
                  name={["filter", "offsetDateTimeNotNullMin"]}
                  label="Offset Date Time Not Null Min"
                >
                  <DatePicker showTime={{ format: "HH:mm:ss" }} />
                </Form.Item>
              </Col>

              <Col span={6} style={{ display: showAll ? "block" : "none" }}>
                <Form.Item
                  name={["filter", "offsetTimeNotNullMax"]}
                  label="Offset Time Not Null Max"
                >
                  <TimePicker />
                </Form.Item>
              </Col>

              <Col span={6} style={{ display: showAll ? "block" : "none" }}>
                <Form.Item
                  name={["filter", "stringNotNull"]}
                  label="String Not Null"
                >
                  <Input
                    suffix={
                      form.isFieldTouched(["filter", "stringNotNull"]) ? (
                        <CloseCircleOutlined
                          onClick={() =>
                            form.resetFields([["filter", "stringNotNull"]])
                          }
                        />
                      ) : (
                        <span />
                      )
                    }
                  />
                </Form.Item>
              </Col>

              <Col span={6} style={{ display: showAll ? "block" : "none" }}>
                <Form.Item name={["filter", "urlNotNull"]} label="Url Not Null">
                  <Input
                    type="url"
                    suffix={
                      form.isFieldTouched(["filter", "urlNotNull"]) ? (
                        <CloseCircleOutlined
                          onClick={() =>
                            form.resetFields([["filter", "urlNotNull"]])
                          }
                        />
                      ) : (
                        <span />
                      )
                    }
                  />
                </Form.Item>
              </Col>
            </Row>
          );
        }}
      </Form.Item>

      <Row justify="space-between">
        <Col>
          <Space>
            <Button type="primary" htmlType="submit">
              <FormattedMessage id="filters.apply" />
            </Button>
            <Button onClick={onResetFilters}>
              <FormattedMessage id="filters.reset" />
            </Button>
          </Space>
        </Col>

        <Col>
          <Button type="link" onClick={() => setShowAll(!showAll)}>
            <Space>
              <FormattedMessage
                id={showAll ? "filters.collapse" : "filters.showAll"}
              />
              {countHiddenInvalideFilters > 0 && (
                <Badge
                  style={{ backgroundColor: "#FF4D4F" }}
                  size="small"
                  count={countHiddenInvalideFilters}
                />
              )}
              {countHiddenInvalideFilters === 0 &&
                countHiddenTouchedFilters > 0 && (
                  <Badge
                    style={{ backgroundColor: "#1890ff" }}
                    size="small"
                    count={countHiddenTouchedFilters}
                  />
                )}
              {showAll ? <UpOutlined /> : <DownOutlined />}
            </Space>
          </Button>
        </Col>
      </Row>
    </Form>
  );
}

interface ItemCardsListProps {
  items?: ItemListType;
  loading?: boolean;
  error?: ApolloError;
}

/**
 * Collection of cards, each card representing an item
 */
function Cards({ items, loading, error }: ItemCardsListProps) {
  if (loading) {
    return <Spin />;
  }

  if (error) {
    return <RequestFailedError />;
  }

  if (items == null || items.length === 0) {
    return <Empty />;
  }

  return (
    <Space direction="vertical" className="card-space">
      {items.map(item => (
        <ItemCard item={item} key={item?.id} />
      ))}
    </Space>
  );
}

function ItemCard({ item }: { item: ItemType }) {
  // Get the action buttons that will be displayed in the card
  const cardActions: ReactNode[] = useCardActions(item);

  if (item == null) {
    return null;
  }

  return (
    <Card
      key={item.id}
      title={getNotNullScalarsTestEntityDisplayName(item)}
      actions={cardActions}
      className="narrow-layout"
    >
      <ValueWithLabel
        key="bigDecimalNotNull"
        label="Big Decimal Not Null"
        value={item.bigDecimalNotNull ?? undefined}
      />
      <ValueWithLabel
        key="bigIntNotNull"
        label="Big Int Not Null"
        value={item.bigIntNotNull ?? undefined}
      />
      <ValueWithLabel
        key="dateTestNotNull"
        label="Date Test Not Null"
        value={item.dateTestNotNull?.format("LLL") ?? undefined}
      />
      <ValueWithLabel
        key="localDateNotNull"
        label="Local Date Not Null"
        value={item.localDateNotNull?.format("LL") ?? undefined}
      />
      <ValueWithLabel
        key="localDateTimeNotNull"
        label="Local Date Time Not Null"
        value={item.localDateTimeNotNull?.format("LLL") ?? undefined}
      />
      <ValueWithLabel
        key="localTimeNotNull"
        label="Local Time Not Null"
        value={item.localTimeNotNull?.format("LTS") ?? undefined}
      />
      <ValueWithLabel
        key="offsetDateTimeNotNull"
        label="Offset Date Time Not Null"
        value={item.offsetDateTimeNotNull?.format("LLL") ?? undefined}
      />
      <ValueWithLabel
        key="offsetTimeNotNull"
        label="Offset Time Not Null"
        value={item.offsetTimeNotNull?.format("LTS") ?? undefined}
      />
      <ValueWithLabel
        key="stringNotNull"
        label="String Not Null"
        value={item.stringNotNull ?? undefined}
      />
      <ValueWithLabel
        key="urlNotNull"
        label="Url Not Null"
        value={item.urlNotNull ?? undefined}
        isUrl={true}
      />
    </Card>
  );
}

/**
 * Returns action buttons that will be displayed inside the card.
 */
function useCardActions(item: ItemType): ReactNode[] {
  const intl = useIntl();
  const navigate = useNavigate();
  const { showDeleteConfirm, deleting } = useDeleteConfirm(item?.id);

  return [
    <EditOutlined
      key="edit"
      title={intl.formatMessage({ id: "common.edit" })}
      onClick={() => {
        if (item?.id != null) {
          navigate(item.id);
        }
      }}
    />,
    deleting ? (
      <LoadingOutlined />
    ) : (
      <DeleteOutlined
        key="delete"
        title={intl.formatMessage({ id: "common.remove" })}
        onClick={showDeleteConfirm}
      />
    )
  ];
}

/**
 * Returns a confirmation dialog and invokes delete mutation upon confirmation
 * @param id id of the entity instance that should be deleted
 */
function useDeleteConfirm(id: string | null | undefined) {
  const intl = useIntl();

  const [runDeleteMutation, { loading }] = useMutation(
    DELETE_NOT_NULL_SCALARS_TEST_ENTITY
  );
  const deleteItem = useDeleteItem(id, runDeleteMutation, REFETCH_QUERIES);

  // Callback that deletes the item
  function handleDeleteItem() {
    deleteItem()
      .then(({ errors }: FetchResult) => {
        if (errors == null || errors.length === 0) {
          return handleDeleteSuccess();
        }
        return handleDeleteGraphQLError(errors);
      })
      .catch(handleDeleteNetworkError);
  }

  // Function that is executed when mutation is successful
  function handleDeleteSuccess() {
    return message.success(
      intl.formatMessage({ id: "EntityDetailsScreen.deletedSuccessfully" })
    );
  }

  // Function that is executed when mutation results in a GraphQL error
  function handleDeleteGraphQLError(
    errors: ReadonlyArray<GraphQLError> | undefined
  ) {
    console.error(errors);
    return message.error(intl.formatMessage({ id: "common.requestFailed" }));
  }

  // Function that is executed when mutation results in a network error (such as 4xx or 5xx)
  function handleDeleteNetworkError(error: Error | ApolloError) {
    console.error(error);
    return message.error(intl.formatMessage({ id: "common.requestFailed" }));
  }

  return {
    showDeleteConfirm: () =>
      Modal.confirm({
        content: intl.formatMessage({
          id: "EntityListScreen.deleteConfirmation"
        }),
        okText: intl.formatMessage({ id: "common.ok" }),
        cancelText: intl.formatMessage({ id: "common.cancel" }),
        onOk: handleDeleteItem
      }),
    deleting: loading
  };
}

function stateToSearchParams(
  state: QueryVariablesType
): Record<string, string> {
  const { page, sort, ...filter } = state;
  const params: Record<string, string> = {};

  if (page != null) {
    params.pageNumber = String(page.number + 1);
    params.pageSize = String(page.size);
  }

  if (sort != null && !Array.isArray(sort)) {
    params.sortProperty = String(sort.property);
    params.sortDirection = String(sort.direction);
  }

  if (filter != null && Object.keys(filter).length > 0) {
    params.filter = JSON.stringify(filter);
  }

  return params;
}

function searchParamsToState(
  searchParams: URLSearchParams
): QueryVariablesType {
  let state: QueryVariablesType = {};
  const {
    pageNumber,
    pageSize,
    sortProperty,
    sortDirection,
    filter
  } = Object.fromEntries(searchParams.entries());

  if (pageNumber != null && pageSize != null) {
    state.page = {
      number: Number(pageNumber) - 1,
      size: Number(pageSize)
    };
  } else {
    state.page = {
      number: 0,
      size: DEFAULT_PAGE_SIZE
    };
  }

  if (sortProperty != null && sortDirection != null) {
    state.sort = {
      direction: sortDirection as SortDirection,
      property: sortProperty as NotNullScalarsTestEntityOrderByProperty
    };
  }

  if (filter != null) {
    state = {
      ...state,
      ...JSON.parse(decodeURIComponent(filter))
    };
  }

  return state;
}

function extractFilterParams(state: QueryVariablesType) {
  const { page: _page, sort: _sort, ...filter } = state;
  return filter;
}

/**
 * Type of data object received when executing the query
 */
type QueryResultType = ResultOf<
  typeof NOT_NULL_SCALARS_TEST_ENTITY_LIST_WITH_FILTER_SORT_PAGE
>;
/**
 * Type of variables used to filter the items list
 */
type QueryVariablesType = VariablesOf<
  typeof NOT_NULL_SCALARS_TEST_ENTITY_LIST_WITH_FILTER_SORT_PAGE
>;
/**
 * Type of the items list
 */
type ItemListType = Exclude<
  QueryResultType["notNullScalarsTestEntityList"],
  null | undefined
>["content"];
/**
 * Type of a single item
 */
type ItemType = Exclude<ItemListType, null | undefined>[0];
