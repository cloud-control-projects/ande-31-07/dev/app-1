import { ScalarsTestEntity } from "../../gql/graphql";

export function getScalarsTestEntityDisplayName(
  entityInstance?: ScalarsTestEntity | null
): string {
  if (entityInstance == null) {
    return "";
  }
  if (entityInstance.id != null) {
    return String(entityInstance.id);
  }
  return JSON.stringify(entityInstance);
}
