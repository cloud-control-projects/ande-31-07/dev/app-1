package io.jmix2mvp.petclinic.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.springframework.data.domain.Sort;

public class OwnerOrderByInput {

    private final OwnerOrderByProperty property;
    private final Sort.Direction direction;

    @JsonCreator
    public OwnerOrderByInput(OwnerOrderByProperty property, Sort.Direction direction) {
        this.property = property;
        this.direction = direction;
    }

    public OwnerOrderByProperty getProperty() {
        return property;
    }

    public Sort.Direction getDirection() {
        return direction;
    }

    @Override
    public String toString() {
        return "OwnerOrderByInput{" +
                "field=" + property +
                ", direction=" + direction +
                '}';
    }
}
