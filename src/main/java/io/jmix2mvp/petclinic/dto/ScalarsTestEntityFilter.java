package io.jmix2mvp.petclinic.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.time.*;
import java.util.Date;

public class ScalarsTestEntityFilter {

    private Integer intTest;
    private Byte byteTest;
    private Short shortTest;
    private Double doubleTest;
    private Float floatTest;
    private String string;
    private Boolean bool;
    private BigInteger bigInt;
    private Long longTest;
    private BigDecimal bigDecimal;
    private LocalDate localDate;
    private OffsetTime offsetTime;
    private OffsetDateTime offsetDateTime;
    private LocalTime localTime;
    private LocalDateTime localDateTime;
    private Date dateTest;
    private URL url;
//    private int intPrimitive;
//    private byte bytePrimitive;
//    private short shortPrimitive;
//    private double doublePrimitive;
//    private float floatPrimitive;
//    private boolean boolPrimitive;
//    private long longPrimitive;

    public Integer getIntTest() {
        return intTest;
    }

    public void setIntTest(Integer intTest) {
        this.intTest = intTest;
    }

    public Byte getByteTest() {
        return byteTest;
    }

    public void setByteTest(Byte byteTest) {
        this.byteTest = byteTest;
    }

    public Short getShortTest() {
        return shortTest;
    }

    public void setShortTest(Short shortTest) {
        this.shortTest = shortTest;
    }

    public Double getDoubleTest() {
        return doubleTest;
    }

    public void setDoubleTest(Double doubleTest) {
        this.doubleTest = doubleTest;
    }

    public Float getFloatTest() {
        return floatTest;
    }

    public void setFloatTest(Float floatTest) {
        this.floatTest = floatTest;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public Boolean getBool() {
        return bool;
    }

    public void setBool(Boolean bool) {
        this.bool = bool;
    }

    public BigInteger getBigInt() {
        return bigInt;
    }

    public void setBigInt(BigInteger bigInt) {
        this.bigInt = bigInt;
    }

    public Long getLongTest() {
        return longTest;
    }

    public void setLongTest(Long longTest) {
        this.longTest = longTest;
    }

    public BigDecimal getBigDecimal() {
        return bigDecimal;
    }

    public void setBigDecimal(BigDecimal bigDecimal) {
        this.bigDecimal = bigDecimal;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    public OffsetTime getOffsetTime() {
        return offsetTime;
    }

    public void setOffsetTime(OffsetTime offsetTime) {
        this.offsetTime = offsetTime;
    }

    public OffsetDateTime getOffsetDateTime() {
        return offsetDateTime;
    }

    public void setOffsetDateTime(OffsetDateTime offsetDateTime) {
        this.offsetDateTime = offsetDateTime;
    }

    public LocalTime getLocalTime() {
        return localTime;
    }

    public void setLocalTime(LocalTime localTime) {
        this.localTime = localTime;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }

    public Date getDateTest() {
        return dateTest;
    }

    public void setDateTest(Date dateTest) {
        this.dateTest = dateTest;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

//    public int getIntPrimitive() {
//        return intPrimitive;
//    }
//
//    public void setIntPrimitive(int intPrimitive) {
//        this.intPrimitive = intPrimitive;
//    }
//
//    public byte getBytePrimitive() {
//        return bytePrimitive;
//    }
//
//    public void setBytePrimitive(byte bytePrimitive) {
//        this.bytePrimitive = bytePrimitive;
//    }
//
//    public short getShortPrimitive() {
//        return shortPrimitive;
//    }
//
//    public void setShortPrimitive(short shortPrimitive) {
//        this.shortPrimitive = shortPrimitive;
//    }
//
//    public double getDoublePrimitive() {
//        return doublePrimitive;
//    }
//
//    public void setDoublePrimitive(double doublePrimitive) {
//        this.doublePrimitive = doublePrimitive;
//    }
//
//    public float getFloatPrimitive() {
//        return floatPrimitive;
//    }
//
//    public void setFloatPrimitive(float floatPrimitive) {
//        this.floatPrimitive = floatPrimitive;
//    }
//
//    public boolean getBoolPrimitive() {
//        return boolPrimitive;
//    }
//
//    public void setBoolPrimitive(boolean boolPrimitive) {
//        this.boolPrimitive = boolPrimitive;
//    }
//
//    public long getLongPrimitive() {
//        return longPrimitive;
//    }
//
//    public void setLongPrimitive(long longPrimitive) {
//        this.longPrimitive = longPrimitive;
//    }
}
