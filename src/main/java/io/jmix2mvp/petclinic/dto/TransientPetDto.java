package io.jmix2mvp.petclinic.dto;

import io.jmix2mvp.petclinic.entity.Pet;

import java.time.LocalDate;
import java.util.List;

/**
 * DTO for {@link Pet}
 */
public class TransientPetDto extends BaseDTO {
    private String identificationNumber;
    private LocalDate birthDate;
    private Integer weightInGrams;
    private PetTypeDTO type;
    private OwnerDTO owner;
    private List<TagDTO> tags;
    private List<PetDiseaseDTO> diseases;
    private PetDescriptionDTO description;

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getWeightInGrams() {
        return weightInGrams;
    }

    public void setWeightInGrams(Integer weightInGrams) {
        this.weightInGrams = weightInGrams;
    }

    public PetTypeDTO getType() {
        return type;
    }

    public void setType(PetTypeDTO type) {
        this.type = type;
    }

    public OwnerDTO getOwner() {
        return owner;
    }

    public void setOwner(OwnerDTO owner) {
        this.owner = owner;
    }

    public List<TagDTO> getTags() {
        return tags;
    }

    public void setTags(List<TagDTO> tags) {
        this.tags = tags;
    }

    public List<PetDiseaseDTO> getDiseases() {
        return diseases;
    }

    public void setDiseases(List<PetDiseaseDTO> diseases) {
        this.diseases = diseases;
    }

    public PetDescriptionDTO getDescription() {
        return description;
    }

    public void setDescription(PetDescriptionDTO description) {
        this.description = description;
    }
}