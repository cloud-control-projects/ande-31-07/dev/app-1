package io.jmix2mvp.petclinic.dto;

import java.time.LocalDateTime;

public class VisitFilter {
    private String ownerFirstName;
    private String ownerLastName;
    private String petIdentificationNumber;
    private LocalDateTime visitStartAfter;
    private LocalDateTime visitStartBefore;
    public String getOwnerFirstName() {
        return ownerFirstName;
    }

    public void setOwnerFirstName(String ownerFirstName) {
        this.ownerFirstName = ownerFirstName;
    }

    public String getOwnerLastName() {
        return ownerLastName;
    }

    public void setOwnerLastName(String ownerLastName) {
        this.ownerLastName = ownerLastName;
    }

    public String getPetIdentificationNumber() {
        return petIdentificationNumber;
    }

    public void setPetIdentificationNumber(String petIdentificationNumber) {
        this.petIdentificationNumber = petIdentificationNumber;
    }

    public LocalDateTime getVisitStartAfter() {
        return visitStartAfter;
    }

    public void setVisitStartAfter(LocalDateTime visitStartAfter) {
        this.visitStartAfter = visitStartAfter;
    }

    public LocalDateTime getVisitStartBefore() {
        return visitStartBefore;
    }

    public void setVisitStartBefore(LocalDateTime visitStartBefore) {
        this.visitStartBefore = visitStartBefore;
    }
}
