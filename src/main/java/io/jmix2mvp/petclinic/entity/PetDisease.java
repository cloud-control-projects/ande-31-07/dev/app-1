package io.jmix2mvp.petclinic.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pet_disease")
public class PetDisease {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pet_disease_identifier", nullable = false)
    private Long petDiseaseIdentifier;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    public Long getPetDiseaseIdentifier() {
        return petDiseaseIdentifier;
    }

    public void setPetDiseaseIdentifier(Long petDiseaseIdentifier) {
        this.petDiseaseIdentifier = petDiseaseIdentifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
