package io.jmix2mvp.petclinic.entity;

import javax.persistence.*;

@Entity
@Table(name = "pet_type")
public class PetType extends BaseEntity {
    @Column(name = "name", nullable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "defense_status")
    private ProtectionStatus defenseStatus;

    public ProtectionStatus getDefenseStatus() {
        return defenseStatus;
    }

    public void setDefenseStatus(ProtectionStatus status) {
        this.defenseStatus = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}