package io.jmix2mvp.petclinic.entity;

import io.jmix2mvp.petclinic.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.time.*;
import java.util.Date;

@Entity
@Table(name = "scalars_test_entity")
public class ScalarsTestEntity extends BaseEntity {

    /* Standard Scalar types */

    @Column(name = "int_test")
    private Integer intTest;

    @Column(name = "byte_test")
    private Byte byteTest;

    @Column(name = "short_test")
    private Short shortTest;

    @Column(name = "double_test")
    private Double doubleTest;

    @Column(name = "float_test")
    private Float floatTest;

    @Column(name = "string")
    private String string;

    @Column(name = "bool")
    private Boolean bool;

    /* ExtendedScalar Types */

    @Column(name = "big_int")
    private BigInteger bigInt;

    @Column(name = "long_test")
    private Long longTest;

    @Column(name = "big_decimal", precision = 19, scale = 2)
    private BigDecimal bigDecimal;

    @Column(name = "local_date")
    private LocalDate localDate;

    @Column(name = "offset_time")
    private OffsetTime offsetTime;

    @Column(name = "offset_date_time")
    private OffsetDateTime offsetDateTime;

    @Column(name = "local_time")
    private LocalTime localTime;

    @Column(name = "local_date_time")
    private LocalDateTime localDateTime;

    @Column(name = "date_test")
    private Date dateTest;

    @Column(name = "url")
    private URL url;

    /* Primitives */

    @Column(name = "int_primitive")
    private int intPrimitive;

    @Column(name = "byte_primitive")
    private byte bytePrimitive;

    @Column(name = "short_primitive")
    private short shortPrimitive;

    @Column(name = "double_primitive")
    private double doublePrimitive;

    @Column(name = "float_primitive")
    private float floatPrimitive;

    @Column(name = "bool_primitive")
    private boolean boolPrimitive;

    @Column(name = "long_primitive")
    private long longPrimitive;


    public Integer getIntTest() {
        return intTest;
    }

    public void setIntTest(Integer intTest) {
        this.intTest = intTest;
    }

    public Byte getByteTest() {
        return byteTest;
    }

    public void setByteTest(Byte byteTest) {
        this.byteTest = byteTest;
    }

    public Short getShortTest() {
        return shortTest;
    }

    public void setShortTest(Short shortTest) {
        this.shortTest = shortTest;
    }

    public Double getDoubleTest() {
        return doubleTest;
    }

    public void setDoubleTest(Double doubleTest) {
        this.doubleTest = doubleTest;
    }

    public Float getFloatTest() {
        return floatTest;
    }

    public void setFloatTest(Float floatTest) {
        this.floatTest = floatTest;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public Boolean getBool() {
        return bool;
    }

    public void setBool(Boolean bool) {
        this.bool = bool;
    }

    public BigInteger getBigInt() {
        return bigInt;
    }

    public void setBigInt(BigInteger bigInt) {
        this.bigInt = bigInt;
    }

    public Long getLongTest() {
        return longTest;
    }

    public void setLongTest(Long longTest) {
        this.longTest = longTest;
    }

    public BigDecimal getBigDecimal() {
        return bigDecimal;
    }

    public void setBigDecimal(BigDecimal bigDecimal) {
        this.bigDecimal = bigDecimal;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    public OffsetTime getOffsetTime() {
        return offsetTime;
    }

    public void setOffsetTime(OffsetTime offsetTime) {
        this.offsetTime = offsetTime;
    }

    public OffsetDateTime getOffsetDateTime() {
        return offsetDateTime;
    }

    public void setOffsetDateTime(OffsetDateTime offsetDateTime) {
        this.offsetDateTime = offsetDateTime;
    }

    public LocalTime getLocalTime() {
        return localTime;
    }

    public void setLocalTime(LocalTime localTime) {
        this.localTime = localTime;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }

    public Date getDateTest() {
        return dateTest;
    }

    public void setDateTest(Date dateTest) {
        this.dateTest = dateTest;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public int getIntPrimitive() {
        return intPrimitive;
    }

    public void setIntPrimitive(int intPrimitive) {
        this.intPrimitive = intPrimitive;
    }

    public byte getBytePrimitive() {
        return bytePrimitive;
    }

    public void setBytePrimitive(byte bytePrimitive) {
        this.bytePrimitive = bytePrimitive;
    }

    public short getShortPrimitive() {
        return shortPrimitive;
    }

    public void setShortPrimitive(short shortPrimitive) {
        this.shortPrimitive = shortPrimitive;
    }

    public double getDoublePrimitive() {
        return doublePrimitive;
    }

    public void setDoublePrimitive(double doublePrimitive) {
        this.doublePrimitive = doublePrimitive;
    }

    public float getFloatPrimitive() {
        return floatPrimitive;
    }

    public void setFloatPrimitive(float floatPrimitive) {
        this.floatPrimitive = floatPrimitive;
    }

    public boolean isBoolPrimitive() {
        return boolPrimitive;
    }

    public void setBoolPrimitive(boolean boolPrimitive) {
        this.boolPrimitive = boolPrimitive;
    }

    public long getLongPrimitive() {
        return longPrimitive;
    }

    public void setLongPrimitive(long longPrimitive) {
        this.longPrimitive = longPrimitive;
    }
}