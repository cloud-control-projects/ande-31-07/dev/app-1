package io.jmix2mvp.petclinic.graphql;

import com.amplicode.core.graphql.annotation.GraphQLId;
import io.jmix2mvp.petclinic.repository.ClientValidationTestEntityRepository;
import io.jmix2mvp.petclinic.entity.ClientValidationTestEntity;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Controller
public class ClientValidationTestEntityController {
    private final ClientValidationTestEntityRepository crudRepository;

    public ClientValidationTestEntityController(ClientValidationTestEntityRepository crudRepository) {
        this.crudRepository = crudRepository;
    }

    @MutationMapping(name = "deleteClientValidationTestEntity")
    @Transactional
    public void delete(@GraphQLId @Argument @NonNull Long id) {
        ClientValidationTestEntity entity = crudRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Unable to find entity by id: %s ", id)));

        crudRepository.delete(entity);
    }

    @QueryMapping(name = "clientValidationTestEntityList")
    @Transactional(readOnly = true)
    @NonNull
    public List<ClientValidationTestEntity> findAll() {
        return crudRepository.findAll();
    }

    @QueryMapping(name = "clientValidationTestEntity")
    @Transactional(readOnly = true)
    @NonNull
    public ClientValidationTestEntity findById(@GraphQLId @Argument @NonNull Long id) {
        return crudRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Unable to find entity by id: %s ", id)));
    }

    @MutationMapping(name = "updateClientValidationTestEntity")
    @Transactional
    @NonNull
    public ClientValidationTestEntity update(@Argument @NonNull ClientValidationTestEntity input) {
        if (input.getId() != null) {
            if (!crudRepository.existsById(input.getId())) {
                throw new RuntimeException(
                        String.format("Unable to find entity by id: %s ", input.getId()));
            }
        }
        return crudRepository.save(input);
    }
}
