package io.jmix2mvp.petclinic.graphql;

import com.amplicode.core.graphql.annotation.GraphQLId;
import com.amplicode.core.graphql.paging.OffsetPageInput;
import com.amplicode.core.graphql.paging.ResultPage;
import io.jmix2mvp.petclinic.dto.*;
import io.jmix2mvp.petclinic.entity.Owner;
import io.jmix2mvp.petclinic.mapper.DTOMapper;
import io.jmix2mvp.petclinic.repository.OwnerRepository;
import io.jmix2mvp.petclinic.repository.OwnerSpecifications;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.lang.NonNull;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static io.jmix2mvp.petclinic.Authorities.ADMIN;
import static io.jmix2mvp.petclinic.Authorities.VETERINARIAN;
import static io.jmix2mvp.petclinic.repository.OwnerSpecifications.byFirstNameContains;
import static io.jmix2mvp.petclinic.repository.OwnerSpecifications.byLastNameContains;

@Controller
public class OwnerController {

    public static final int DEFAULT_PAGE_SIZE = 10; //TODO app property

    private final OwnerRepository crudRepository;
    private final DTOMapper mapper;

    public OwnerController(OwnerRepository crudRepository, DTOMapper mapper) {
        this.crudRepository = crudRepository;
        this.mapper = mapper;
    }

    @Secured({ADMIN, VETERINARIAN})
    @QueryMapping(name = "owner")
    @Transactional
    public OwnerDTO findById(@GraphQLId @Argument Long id) {
        return crudRepository.findById(id)
                .map(mapper::ownerToDTO)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Unable to find entity by id: %s ", id)));
    }

    @Secured({ADMIN, VETERINARIAN})
    @QueryMapping(name = "ownerListByIds")
    @Transactional
    public List<OwnerDTO> findAll(@NonNull @Argument List<@GraphQLId Long> ids) {
        return crudRepository.findAllById(ids).stream()
                .map(mapper::ownerToDTO)
                .collect(Collectors.toList());
    }

    @Secured({ADMIN, VETERINARIAN})
    @QueryMapping(name = "ownerListFull")
    @Transactional
    public List<OwnerDTO> findAll() {
        return crudRepository.findAll().stream()
                .map(mapper::ownerToDTO)
                .collect(Collectors.toList());
    }

    @Secured({ADMIN, VETERINARIAN})
    @QueryMapping(name = "ownerByNamesList")
    @Transactional
    public List<OwnerDTO> findAll(@Argument OwnerFilter filter) {
        Specification<Owner> specification = Specification.where(null);
        if (filter != null) {
            if (filter.getFirstName() != null) {
                specification = specification.and(byFirstNameContains(filter.getFirstName()));
            }
            if (filter.getLastName() != null) {
                specification = specification.and(byLastNameContains(filter.getLastName()));
            }
        }
        return crudRepository.findAll(specification).stream()
                .map(mapper::ownerToDTO)
                .collect(Collectors.toList());
    }

    @Secured({ADMIN, VETERINARIAN})
    @QueryMapping(name = "ownerListOffsetPage")
    @Transactional
    public ResultPage<OwnerDTO> ownerListOffsetPage(@Argument OffsetPageInput page) {
        PageRequest pageRequest = createPageRequest(page);
        Page<Owner> resultPage = crudRepository.findAll(pageRequest);
        long totalElements = resultPage.getTotalElements();
        List<OwnerDTO> content = resultPage.getContent().stream()
                .map(mapper::ownerToDTO)
                .collect(Collectors.toList());
        return ResultPage.page(content, totalElements);
    }

    @Secured({ADMIN, VETERINARIAN})
    @QueryMapping(name = "ownerListSorted")
    @Transactional
    public List<OwnerDTO> ownerListSorted(@Argument List<OwnerOrderByInput> sort) {
        Sort sortRequest = createSort(sort);
        return crudRepository.findAll(sortRequest).stream()
                .map(mapper::ownerToDTO)
                .collect(Collectors.toList());
    }

    @Secured({ADMIN, VETERINARIAN})
    @QueryMapping(name = "ownerListByNamesFilter")
    @Transactional
    public List<OwnerDTO> ownerListByNamesFilter(@Argument OwnerFilter filter) {
        Specification<Owner> specification = OwnerSpecifications.byNamesStartsWith(filter);
        return crudRepository.findAll(specification).stream()
                .map(mapper::ownerToDTO)
                .collect(Collectors.toList());
    }

    @Secured({ADMIN, VETERINARIAN})
    @QueryMapping(name = "ownerListOffsetPageSorted")
    @Transactional
    public ResultPage<OwnerDTO> ownerListOffsetPageSorted(@Argument OffsetPageInput page,
                                               @Argument List<OwnerOrderByInput> sort) {
        PageRequest pageRequest = createPageRequest(page, sort);
        Page<Owner> resultPage = crudRepository.findAll(pageRequest);
        long totalElements = resultPage.getTotalElements();
        List<OwnerDTO> content = resultPage.getContent().stream()
                .map(mapper::ownerToDTO)
                .collect(Collectors.toList());
        return ResultPage.page(content, totalElements);
    }

    @Secured({ADMIN, VETERINARIAN})
    @QueryMapping(name = "ownerListByNamesFilterOffsetPage")
    @Transactional
    public ResultPage<OwnerDTO> ownerListByNamesFilterOffsetPage(@Argument OffsetPageInput page,
                                                      @Argument OwnerFilter filter) {
        Specification<Owner> specification = OwnerSpecifications.byNamesStartsWith(filter);
        PageRequest pageRequest = createPageRequest(page);
        Page<Owner> resultPage = crudRepository.findAll(specification, pageRequest);
        long totalElements = resultPage.getTotalElements();
        List<OwnerDTO> content = resultPage.getContent().stream()
                .map(mapper::ownerToDTO)
                .collect(Collectors.toList());
        return ResultPage.page(content, totalElements);
    }

    @Secured({ADMIN, VETERINARIAN})
    @QueryMapping(name = "ownerListByNamesFilterSorted")
    @Transactional
    public List<OwnerDTO> ownerListByNamesFilterSorted(@Argument List<OwnerOrderByInput> sort,
                                                       @Argument OwnerFilter filter) {
        Specification<Owner> specification = OwnerSpecifications.byNamesStartsWith(filter);
        Sort sortRequest = createSort(sort);
        return crudRepository.findAll(specification, sortRequest).stream()
                .map(mapper::ownerToDTO)
                .collect(Collectors.toList());
    }

    @Secured({ADMIN, VETERINARIAN})
    @QueryMapping(name = "ownerList")
    @Transactional
    public ResultPage<OwnerDTO> ownerListWithAllOptions(@Argument OffsetPageInput page,
                                                        @Argument List<OwnerOrderByInput> sort,
                                                        @Argument OwnerFilter filter) {
        Specification<Owner> specification = OwnerSpecifications.byNamesStartsWith(filter);
        PageRequest pageRequest = createPageRequest(page, sort);
        Page<Owner> resultPage = crudRepository.findAll(specification, pageRequest);
        long totalElements = resultPage.getTotalElements();
        List<OwnerDTO> content = resultPage.getContent().stream()
                .map(mapper::ownerToDTO)
                .collect(Collectors.toList());
        return ResultPage.page(content, totalElements);
    }

    @NonNull
    @QueryMapping(name = "ownerListWithResultPage")
    public ResultPage<Owner> ownerListWithResultPage(@Argument OffsetPageInput page) {
        Pageable pageable = Optional.ofNullable(page)
                .map(p -> PageRequest.of(p.getNumber(), p.getSize()))
                .orElseGet(() -> PageRequest.ofSize(20));
        Page<Owner> result = crudRepository.findAll(pageable);
        return ResultPage.page(result.getContent(), result.getTotalElements());
    }

    @Secured({ADMIN, VETERINARIAN})
    @MutationMapping(name = "updateOwner")
    @Transactional
    public OwnerDTO update(@Argument @Valid OwnerInputDTO input) {
        if (input.getId() != null) {
            if (!crudRepository.existsById(input.getId())) {
                throw new ResourceNotFoundException(
                        String.format("Unable to find entity by id: %s ", input.getId()));
            }
        }

        Owner entity = new Owner();
        mapper.ownerDTOToEntity(input, entity);
        entity = crudRepository.save(entity);

        return mapper.ownerToDTO(entity);
    }

    @Secured({ADMIN, VETERINARIAN})
    @MutationMapping(name = "deleteOwner")
    @Transactional
    public void delete(@GraphQLId @NonNull @Argument Long id) {
        Owner entity = crudRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Unable to find entity by id: %s ", id)));

        crudRepository.delete(entity);
    }

    @Secured({ADMIN, VETERINARIAN})
    @MutationMapping(name = "deleteManyOwner")
    @Transactional
    public void deleteMany(@NonNull @Argument List<@GraphQLId Long> ids) {
        crudRepository.deleteAllById(ids);
    }

    protected PageRequest createPageRequest(OffsetPageInput page) {
        return createPageRequest(page, Collections.emptyList());
    }

    protected PageRequest createPageRequest(OffsetPageInput page, List<OwnerOrderByInput> sortInput) {
        int pageNumber;
        int pageSize;
        if (page == null) {
            pageNumber = 0;
            pageSize = DEFAULT_PAGE_SIZE;
        } else {
            pageNumber = page.getNumber();
            pageSize = page.getSize() > 0 ? page.getSize() : DEFAULT_PAGE_SIZE;
        }

        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize);
        Sort sort = createSort(sortInput);
        return pageRequest.withSort(sort);
    }

    protected Sort createSort(List<OwnerOrderByInput> sortInput) {
        if (sortInput != null && !sortInput.isEmpty()) {
            List<Sort.Order> orders = sortInput.stream()
                    .map(item -> {
                        Sort.Direction direction = item.getDirection() == null
                                ? Sort.DEFAULT_DIRECTION
                                : item.getDirection();
                        switch (item.getProperty()) {
                            case CITY:
                                return Sort.Order.by("city").with(direction);
                            case FIRST_NAME:
                                return Sort.Order.by("firstName").with(direction);
                            case LAST_NAME:
                                return Sort.Order.by("lastName").with(direction);
                            default:
                                return null;
                        }
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            return Sort.by(orders);
        } else {
            return Sort.unsorted();
        }
    }
}
