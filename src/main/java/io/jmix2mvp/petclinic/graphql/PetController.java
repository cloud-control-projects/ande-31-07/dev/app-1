package io.jmix2mvp.petclinic.graphql;

import com.amplicode.core.graphql.annotation.GraphQLId;
import io.jmix2mvp.petclinic.dto.PetDTO;
import io.jmix2mvp.petclinic.dto.PetInputDTO;
import io.jmix2mvp.petclinic.entity.Pet;
import io.jmix2mvp.petclinic.mapper.DTOMapper;
import io.jmix2mvp.petclinic.repository.PetRepository;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.lang.NonNull;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.stream.Collectors;

import static io.jmix2mvp.petclinic.Authorities.ADMIN;
import static io.jmix2mvp.petclinic.Authorities.VETERINARIAN;

@Controller
public class PetController {
    private final PetRepository crudRepository;
    private final DTOMapper mapper;
    @PersistenceContext
    private EntityManager entityManager;

    public PetController(PetRepository crudRepository, DTOMapper mapper) {
        this.crudRepository = crudRepository;
        this.mapper = mapper;
    }

    @Secured({ADMIN, VETERINARIAN})
    @QueryMapping(name = "pet")
    @Transactional
    public PetDTO findById(@GraphQLId @Argument Long id) {
        return crudRepository.findById(id)
                .map(mapper::petToDTO)
                .orElse(null);
    }

    @Secured({ADMIN, VETERINARIAN})
    @QueryMapping(name = "petList")
    @Transactional
    public List<PetDTO> findAll() {
        return crudRepository.findAll().stream()
                .map(mapper::petToDTO)
                .collect(Collectors.toList());
    }

    @Secured({ADMIN, VETERINARIAN})
    @QueryMapping(name = "petByIdentificationNumberList")
    @Transactional
    public List<PetDTO> findAllByIdentificationNumber(@Argument String identificationNumber) {
        List<Pet> result = null;
        if (identificationNumber != null) {
            result = crudRepository.findByIdentificationNumberStartsWith(identificationNumber);
        } else {
            result = crudRepository.findAll();
        }
        return result.stream()
                .map(mapper::petToDTO)
                .collect(Collectors.toList());
    }

    @PreAuthorize("hasAnyRole('ADMIN', 'VETERINARIAN')")
    @MutationMapping(name = "updatePet")
    @Transactional
    public PetDTO update(@Argument PetInputDTO input) {
        if (input.getId() != null) {
            if (!crudRepository.existsById(input.getId())) {
                throw new ResourceNotFoundException(
                        String.format("Unable to find entity by id: %s ", input.getId()));
            }
        }
        Pet entity = new Pet();
        mapper.petDTOToEntity(input, entity);
        entity = crudRepository.saveAndFlush(entity);

        entityManager.refresh(entity);

        return mapper.petToDTO(entity);
    }

    @Secured({ADMIN, VETERINARIAN})
    @MutationMapping(name = "deletePet")
    @Transactional
    public void delete(@GraphQLId @NonNull @Argument Long id) {
        Pet entity = crudRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Unable to find entity by id: %s ", id)));

        crudRepository.delete(entity);
    }

    @Secured({ADMIN, VETERINARIAN})
    @MutationMapping(name = "deleteManyPet")
    @Transactional
    public void deleteAllById(@NonNull @Argument List<@GraphQLId Long> ids) {
        crudRepository.deleteAllById(ids);
    }

    @Secured({ADMIN, VETERINARIAN})
    @QueryMapping(name = "petListByIds")
    @Transactional
    public List<PetDTO> findAllById(@NonNull @Argument List<@GraphQLId Long> ids) {
        return crudRepository.findAllById(ids).stream().map(mapper::petToDTO).collect(Collectors.toList());
    }

    @Secured({ADMIN, VETERINARIAN})
    @MutationMapping(name = "updateManyPet")
    @Transactional
    public List<PetDTO> updateMany(@NonNull @Argument List<PetInputDTO> inputList) {
        List<Pet> entities = inputList.stream()
                .map(input -> {
                    Pet entity = new Pet();
                    mapper.petDTOToEntity(input, entity);
                    return entity;
                })
                .collect(Collectors.toList());

        entities = crudRepository.saveAllAndFlush(entities);

        return entities.stream().map(mapper::petToDTO).collect(Collectors.toList());
    }

    @Secured({ADMIN, VETERINARIAN})
    @QueryMapping(name = "petListByTypeId")
    @Transactional
    public List<PetDTO> petListByTypeId(@GraphQLId @NonNull @Argument Long typeId) {
        return crudRepository.findByType_IdIs(typeId).stream()
                .map(mapper::petToDTO)
                .collect(Collectors.toList());
    }
}
