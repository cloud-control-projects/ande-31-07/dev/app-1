package io.jmix2mvp.petclinic.repository;

import io.jmix2mvp.petclinic.entity.Pet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface PetRepository extends JpaRepository<Pet, Long> {
    @Query("select p from Visit v left join v.pet p where v.id = :visitId")
    Optional<Pet> findPetByVisit(@Param("visitId") Long visitId);

    @Query("select p from Pet p where p.identificationNumber like concat(:identificationNumber, '%')")
    List<Pet> findByIdentificationNumberStartsWith(@Param("identificationNumber") String identificationNumber);

    List<Pet> findByType_IdIs(Long id);
}