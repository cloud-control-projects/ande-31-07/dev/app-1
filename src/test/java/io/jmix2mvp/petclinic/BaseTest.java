package io.jmix2mvp.petclinic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.jdbc.JdbcTestUtils;

@SpringBootTest
@ActiveProfiles("test")
public class BaseTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    protected void clearAllTables() {
        JdbcTestUtils.deleteFromTables(jdbcTemplate, "visit", "pet_tags", "pet_diseases", "pet", "owner",
                "pet_disease", "tag", "pet_description", "pet_type");
    }
}
