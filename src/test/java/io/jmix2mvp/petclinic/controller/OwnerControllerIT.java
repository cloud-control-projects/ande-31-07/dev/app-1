package io.jmix2mvp.petclinic.controller;

import com.amplicode.core.graphql.paging.OffsetPageInput;
import com.amplicode.core.graphql.paging.ResultPageImpl;
import io.jmix2mvp.petclinic.BaseTest;
import io.jmix2mvp.petclinic.dto.*;
import io.jmix2mvp.petclinic.entity.Owner;
import io.jmix2mvp.petclinic.mapper.DTOMapper;
import io.jmix2mvp.petclinic.repository.OwnerRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.graphql.tester.AutoConfigureHttpGraphQlTester;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Sort;
import org.springframework.graphql.execution.ErrorType;
import org.springframework.graphql.test.tester.GraphQlTester;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static io.jmix2mvp.petclinic.Authorities.ADMIN;
import static org.assertj.core.api.Assertions.assertThat;

@AutoConfigureHttpGraphQlTester
@WithMockUser(authorities = {ADMIN})
public class OwnerControllerIT extends BaseTest {
    private static final ParameterizedTypeReference<ResultPageImpl<OwnerDTO>> OWNER_DTO_RESULT_PAGE_TYPE_REF = new ParameterizedTypeReference<>() {
    };

    @Autowired
    private GraphQlTester graphQlTester;

    @Autowired
    private DTOMapper dtoMapper;

    @Autowired
    private OwnerRepository ownerRepository;

    @BeforeEach
    void setUp() {
        //remove sample data
        clearAllTables();
    }

    @AfterEach
    void tearDown() {
        //cleanup data
        ownerRepository.deleteAll();
    }

    @Test
    public void testGetOwner() {
        //given: initial data
        Owner owner = createOwnerEntity();
        ownerRepository.saveAndFlush(owner);

        //when: send a GraphQL query to get Owner details by id
        //then: check data in the response
        graphQlTester.documentName("owner")
                .variable("id", owner.getId())
                .execute()
                .path("owner")
                .entity(OwnerDTO.class)
                .satisfies(returnedValue -> {
                    assertThat(returnedValue.getId()).isEqualTo(owner.getId());
                });
    }

    @Test
    public void testGetNonExistingOwner() {
        //when: send a GraphQL query to get details about non-existing Owner
        //then: check error in the response
        graphQlTester.documentName("owner")
                .variable("id", Long.MAX_VALUE)
                .execute()
                .errors()
                .satisfy(responseErrors -> {
                    assertThat(responseErrors).hasSize(1);
                    assertThat(responseErrors.get(0).getErrorType()).isEqualTo(ErrorType.INTERNAL_ERROR);
                });
    }

    @Test
    public void testOwnerListByIds() {
        //given: initial data
        Owner owner = createOwnerEntity();
        ownerRepository.saveAndFlush(owner);

        //when: send a GraphQL query
        //then: check data in the response
        graphQlTester.documentName("ownerListByIds")
                .variable("ids", List.of(owner.getId()))
                .execute()
                .path("ownerListByIds")
                .entityList(OwnerDTO.class)
                .hasSize(1);
    }

    @Test
    public void testGetAllOwners() {
        //given: initial data
        Owner owner = createOwnerEntity();
        ownerRepository.saveAndFlush(owner);

        //when: send a GraphQL query to get all Owners
        //then: check data in the response
        graphQlTester.documentName("ownerList")
                .execute()
                .path("ownerListFull")
                .entityList(OwnerDTO.class)
                .hasSize(1);
    }

    @Test
    public void testOwnersFoundByFirstName() {
        //given: initial data
        Owner owner = createOwnerEntity();
        owner.setFirstName("firstName1");

        ownerRepository.saveAndFlush(owner);

        OwnerFilter filter = new OwnerFilter();
        filter.setFirstName("firstName1");

        //when: send a GraphQL query to search Owner by firstName
        //then: check Owner is found
        ownerShouldBeFound(filter, owner);
    }

    @Test
    public void testOwnersNotFoundByFirstName() {
        //given: initial data
        Owner owner = createOwnerEntity();
        owner.setFirstName("firstName1");

        ownerRepository.saveAndFlush(owner);

        OwnerFilter filter = new OwnerFilter();
        filter.setFirstName("firstName2");

        //when: send a GraphQL query to search Owner by firstName
        //then: check Owner not found
        ownerShouldNotBeFound(filter);
    }

    @Test
    public void testOwnerListOffsetPage() {
        //given: initial data
        Owner entity = createOwnerEntity();
        ownerRepository.saveAndFlush(entity);

        //when: send a GraphQL query to get a paginated result for Owners
        //then: check data in the response
        graphQlTester.documentName("ownerListOffsetPage")
                .execute()
                .path("ownerListOffsetPage")
                .entity(OWNER_DTO_RESULT_PAGE_TYPE_REF)
                .satisfies(resultPage -> {
                    assertThat(resultPage.getContent())
                            .asList()
                            .hasSize(1);
                    assertThat(resultPage.getTotalElements()).isEqualTo(1);
                });
    }

    @Test
    public void testOwnerListOffsetPagination() {
        //given: initial data
        Owner entity = createOwnerEntity();
        ownerRepository.saveAndFlush(entity);

        OffsetPageInput page1 = new OffsetPageInput(0, 1);
        OffsetPageInput page2 = new OffsetPageInput(1, 1);

        //when: send a GraphQL query to get a paginated result for Owners
        //then: check total count of elements and count of elements for page
        checkResultPage(page1, 1, 1);
        checkResultPage(page2, 0, 1);
    }

    @Test
    public void testGetOwnersSortedByCityDesc() {
        //given: initial data
        Owner owner1 = createOwnerEntity();
        owner1.setCity("city1");

        Owner owner2 = createOwnerEntity();
        owner2.setCity("city2");

        ownerRepository.saveAllAndFlush(List.of(owner1, owner2));

        //when: send a GraphQL query to get sorted Owners
        //then: check that Owners are sorted by city desc in the response
        ownersOrderByInput(OwnerOrderByProperty.CITY, Sort.Direction.DESC, owner2, owner1);
    }

    @Test
    public void testGetOwnersSortedByFirstNameAsc() {
        //given: initial data
        Owner owner1 = createOwnerEntity();
        owner1.setFirstName("firstName2");

        Owner owner2 = createOwnerEntity();
        owner2.setFirstName("firstName1");

        ownerRepository.saveAllAndFlush(List.of(owner1, owner2));

        //when: send a GraphQL query to get sorted Owners
        //then: check that Owners are sorted by firstName asc in the response
        ownersOrderByInput(OwnerOrderByProperty.FIRST_NAME, Sort.Direction.ASC, owner2, owner1);
    }


    /**
     * Sends a GraphQL query to search Owner by specified filter and checks that Owner is found.
     *
     * @param filter        filter to find a value
     * @param expectedOwner expected found value
     */
    private void ownerShouldBeFound(OwnerFilter filter, Owner expectedOwner) {
        graphQlTester.documentName("ownerByNamesList")
                .variable("filter", filter)
                .execute()
                .path("ownerByNamesList")
                .entityList(OwnerDTO.class)
                .satisfies(resultList -> {
                    assertThat(resultList)
                            .hasSize(1)
                            .extracting(BaseDTO::getId)
                            .containsExactly(expectedOwner.getId());
                });
    }

    /**
     * Sends a GraphQL query to get a paginated result for Owners and checks total count of elements and count of elements for page.
     *
     * @param page                  an input GraphQL argument for page
     * @param expectedCount         an expected count of elements for page
     * @param expectedTotalElements an expected total count of elements
     */
    private void checkResultPage(OffsetPageInput page, int expectedCount, int expectedTotalElements) {
        graphQlTester.documentName("ownerListOffsetPage")
                .variable("page", page)
                .execute()
                .path("ownerListOffsetPage")
                .entity(OWNER_DTO_RESULT_PAGE_TYPE_REF)
                .satisfies(resultPage -> {
                    assertThat(resultPage.getContent())
                            .asList()
                            .hasSize(expectedCount);
                    assertThat(resultPage.getTotalElements()).isEqualTo(expectedTotalElements);
                });
    }

    /**
     * Sends a GraphQL query to search Owner by specified filter and checks that Owner is not found.
     *
     * @param filter filter to not to find a value
     */
    private void ownerShouldNotBeFound(OwnerFilter filter) {
        graphQlTester.documentName("ownerByNamesList")
                .variable("filter", filter)
                .execute()
                .path("ownerByNamesList")
                .entityList(OwnerDTO.class)
                .satisfies(resultList -> assertThat(resultList).isEmpty());
    }

    /**
     * Sends a GraphQL query to get a sorted list of Owners and checks that response contains all specified Owners in the right order.
     *
     * @param orderByProperty property which the values will be sorted by
     * @param sortDirection   sort direction
     * @param expectedOwners  Owners in the expected order
     */
    private void ownersOrderByInput(OwnerOrderByProperty orderByProperty, Sort.Direction sortDirection, Owner... expectedOwners) {
        OwnerOrderByInput sortInput = new OwnerOrderByInput(orderByProperty, sortDirection);

        graphQlTester.documentName("ownerListSorted")
                .variable("sort", sortInput)
                .execute()
                .path("ownerListSorted")
                .entityList(OwnerDTO.class)
                .satisfies(resultList -> assertThat(resultList)
                        .hasSize(expectedOwners.length)
                        .extracting(BaseDTO::getId)
                        .containsExactlyElementsOf(Arrays.stream(expectedOwners).map(Owner::getId).collect(Collectors.toList())));
    }

    private Owner createOwnerEntity() {
        Owner owner = new Owner();
        owner.setFirstName("firstName");
        owner.setLastName("lastName");
        owner.setAddress("address");
        owner.setCity("city");
        return owner;
    }

}
